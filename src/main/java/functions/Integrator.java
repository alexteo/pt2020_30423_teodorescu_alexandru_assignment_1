package functions;

import model.Monomial;
import model.Polynomial;

public class Integrator {

	public Integrator() {
		// TODO Auto-generated constructor stub
	}
	public Polynomial integratePolynomial(Polynomial el) {
		
		Polynomial polynomial = new Polynomial(el);
		Polynomial result = new Polynomial();
		
		for(Monomial monomial : polynomial.getMonomials()) {
			
			Monomial newMonomial = getIntegratedMonomial(monomial);
			result.addMonomial(newMonomial);
		}
		
		return result;
	}
	
	private Monomial getIntegratedMonomial(Monomial monomial) {
		
		Monomial result = new Monomial(0,0.0);
		
		result.setCoefficient(monomial.getCoefficient() / (monomial.getPower() + 1));
		result.setPower(monomial.getPower() + 1);
		
		return result;
	}
}
