package functions;

import model.*;

public class Divider {

	private Subtractor subtractor = new Subtractor();
	private Multiplier multiplier = new Multiplier();
	
	public Divider() {
		// TODO Auto-generated constructor stub
	}
	
	public DivisionResult dividePolynomials(Polynomial el1, Polynomial el2) {
		
		Polynomial result = new Polynomial();
		Polynomial polynomial1 = new Polynomial(el1);
		Polynomial polynomial2 = new Polynomial(el2);
		Polynomial temp;
		
		if(polynomial2.isZero()) {
			
			return null;
		}
		
		result.setMaxPower(polynomial1.getMaxPower() - polynomial2.getMaxPower());
		
		for (int i = polynomial1.getMaxPower() - polynomial2.getMaxPower(); i >=0 ; i--) {
			
			temp = new Polynomial();
			Double coefficient = polynomial1.getMonomials().getLast().getCoefficient() / polynomial2.getMonomials().getLast().getCoefficient();
			temp.addMonomial(new Monomial(i, coefficient));
			result.addMonomial(new Monomial(i, coefficient));
			
			temp = multiplier.multiplyPolynomials(polynomial2, temp);
			polynomial1 = subtractor.subtractPolynomials(polynomial1, temp);
		}
		
		result = result.revert();
		
		DivisionResult fullResult = new DivisionResult(result, polynomial1);
		
		return fullResult;
	}
}
