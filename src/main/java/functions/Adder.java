package functions;

import java.util.NoSuchElementException;

import model.Monomial;
import model.Polynomial;

public class Adder {

	public Adder() {
		
	}

	public Polynomial addPolynomials(Polynomial el1, Polynomial el2) {
		Polynomial polynomial1 = new Polynomial(el1);
		Polynomial polynomial2 = new Polynomial(el2);
		Polynomial result = new Polynomial();
		Monomial monomial1 = new Monomial(0, 0.0);
		Monomial monomial2 = new Monomial(0, 0.0);
		int i = 0;
		
		if(polynomial1.getMonomials().size() == 0 || polynomial1.isZero()) {
			
			return polynomial2;
		}else if(polynomial2.getMonomials().size() == 0 || polynomial2.isZero()) {
			
			return polynomial1;
		}

		while (polynomial1.getMonomials().size() > 0 || polynomial2.getMonomials().size() > 0) {
		
			result = calcPolynomial(polynomial1, polynomial2, result, monomial1, monomial2, i);
			i++;
		}
		return result;
	}
	
	private Polynomial calcPolynomial(Polynomial polynomial1, Polynomial polynomial2, Polynomial result, Monomial monomial1, Monomial monomial2, int i) {
		
		monomial1 = getPolynomial(polynomial1, monomial1);
		monomial2 = getPolynomial(polynomial2, monomial2);
		
		Monomial resultMonomial = new Monomial(0, 0.0);
		resultMonomial.setPower(i);
		
		if (monomial1.getPower() == i) {
			if (monomial2.getPower() == i) {
				resultMonomial.setCoefficient(monomial1.getCoefficient() + monomial2.getCoefficient());
				result.addMonomial(resultMonomial);
				
				polynomial1 = deleteCurrent(polynomial1);
				polynomial2 = deleteCurrent(polynomial2);
			} else {
				resultMonomial.setCoefficient(monomial1.getCoefficient());
				result.addMonomial(resultMonomial);
				
				polynomial1 = deleteCurrent(polynomial1);
			}
		} else {
			if (monomial2.getPower() == i) {
				resultMonomial.setCoefficient(monomial2.getCoefficient());
				result.addMonomial(resultMonomial);
				
				polynomial2 = deleteCurrent(polynomial2);
			}
		}
		return result;
	}
	private Monomial getPolynomial(Polynomial polynomial, Monomial monomial) {
		if (polynomial.getMonomials().size() > 0) {
			monomial = polynomial.getMonomials().getFirst();
		} else {
			monomial = new Monomial(-1, -1.0);
		}
		return monomial;
	}
	
	private Polynomial deleteCurrent(Polynomial polynomial) {
		try {
			polynomial.getMonomials().remove();
		} catch (NoSuchElementException e) {}
		
		return polynomial;
	}
}
