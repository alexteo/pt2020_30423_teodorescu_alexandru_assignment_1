package functions;

import model.Monomial;
import model.Polynomial;

public class Derivator {

	public Derivator() {
		// TODO Auto-generated constructor stub
	}
	
	public Polynomial derivatePolynomial(Polynomial el) {
		
		Polynomial polynomial = new Polynomial(el);
		Polynomial result = new Polynomial();
		
		for(Monomial monomial : polynomial.getMonomials()) {
			
			Monomial newMonomial = getDerivatedMonomial(monomial);
			
			if(newMonomial != null) {
				
				result.addMonomial(newMonomial);
			}
		}
		
		return result;
	}
	
	private Monomial getDerivatedMonomial(Monomial monomial) {
		
		Monomial result = new Monomial(0,0.0);
		
		if(monomial.getPower() == 0) {
			
			return null;
		}
		
		result.setCoefficient(monomial.getCoefficient() * monomial.getPower());
		result.setPower(monomial.getPower() - 1);
		
		
		return result;
	}
}
