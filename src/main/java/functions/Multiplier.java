package functions;

import java.util.HashMap;

import model.*;

public class Multiplier {

	public Multiplier() {
		// TODO Auto-generated constructor stub
	}
	public Polynomial multiplyPolynomials(Polynomial el1, Polynomial el2) {
		
		Polynomial result = new Polynomial();
		Polynomial polynomial1 = new Polynomial(el1);
		Polynomial polynomial2 = new Polynomial(el2);
		HashMap<Integer, Double> coefficients1 = new HashMap<Integer, Double>();
		HashMap<Integer, Double> coefficients2 = new HashMap<Integer, Double>();
		HashMap<Integer, Double> resultCoefficients = new HashMap<Integer, Double>();
		int i = 0, j = 0;
		
		if(polynomial1.getMonomials().size() == 0) {
			
			return polynomial2;
		}else if(polynomial2.getMonomials().size() == 0) {
			
			return polynomial1;
		}
		
		coefficients1 = getCoefficientList(polynomial1, coefficients1);
		coefficients2 = getCoefficientList(polynomial2, coefficients2);
		
		
		while(i <= polynomial1.getMaxPower()) {
			
			j = 0;
			while(j <= polynomial2.getMaxPower()) {
				
				resultCoefficients = computeCoeff(resultCoefficients, coefficients1, coefficients2, i, j);
				j++;
			}	
			i++;
		}
		
		result = constructResult(polynomial1.getMaxPower() + polynomial2.getMaxPower(), resultCoefficients, result);
		return result;
	}
	
	
	private HashMap<Integer, Double> getCoefficientList(Polynomial polynomial, HashMap<Integer, Double> coefficients){
		
		for(Monomial monomial : polynomial.getMonomials()) {
			
			coefficients.put(monomial.getPower(), monomial.getCoefficient());
		}
		
		return coefficients;
	}
	
	private Polynomial constructResult(int range, HashMap<Integer, Double> resultCoefficients, Polynomial result) {
		
		int k = 0;
		
		while(k <= range) {
		
			if(resultCoefficients.get(k) != 0) {
			
				result.addMonomial(new Monomial(k, resultCoefficients.get(k)));
			}
		
			k++;
		}
		
		return result;
	}

	private HashMap<Integer,Double> computeCoeff(HashMap<Integer,Double> resCoeff, HashMap<Integer,Double> coeff1, HashMap<Integer,Double> coeff2, int i , int j){
		
		Double coef1 = mapToInt(coeff1, i);
		Double coef2 = mapToInt(coeff2, j);
		Double resCoef = mapToInt(resCoeff, i + j);
		
		resCoeff.put(i + j, (coef1 * coef2) + resCoef);
		
		return resCoeff;
	}
	
	private Double mapToInt(HashMap<Integer,Double> map, Integer k) {
		
		Double x;
		if(map.get(k) != null) {
			x = map.get(k);
		}else {
			x = 0.0;
		}
		
		return x;
	}
}
