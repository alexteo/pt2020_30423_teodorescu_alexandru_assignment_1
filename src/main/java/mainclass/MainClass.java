package mainclass;

import javax.swing.JFrame;

import GUI.*;
import functions.*;
import model.*;

public class MainClass {

	public static void main(String[] args) {
		
		View view = new View();
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.pack();
		view.setVisible(true);
	}

}
