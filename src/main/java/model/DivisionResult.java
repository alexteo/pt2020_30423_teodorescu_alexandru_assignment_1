package model;

public class DivisionResult {
	
	private Polynomial quotient;
	private Polynomial remainder;

	public DivisionResult() {
		
	}
	
	public DivisionResult(Polynomial quotient, Polynomial remainder) {
		this.remainder = remainder;
		this.quotient = quotient;
	}
	
	public Polynomial getQuotient() {
		return quotient;
	}

	public void setQuotient(Polynomial quotient) {
		this.quotient = quotient;
	}

	public Polynomial getRemainder() {
		return remainder;
	}

	public void setRemainder(Polynomial remainder) {
		this.remainder = remainder;
	}
	
	public String showDivisionResult() {
		
		String output = "";
		try {
			if(remainder.getMonomials().size() != 0) {
			
				output = "Q: ";
			}
		}catch(NullPointerException e) {}
		
		output += quotient.showPolynomial();
		
		try {
			if(remainder.getMonomials().size() != 0) {
			
				output += " R: ";
				output += remainder.showPolynomial();
			}
		}catch(NullPointerException e) {}
		
		return output;
	}

}
