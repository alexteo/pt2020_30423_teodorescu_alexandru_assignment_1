 package model;

import java.util.LinkedList;
import java.util.regex.*;

public class Polynomial {
	
	private LinkedList<Monomial> monomials = new LinkedList<Monomial>();
	private Integer maxPower = 0;
	
	public Polynomial() {
		
	}
	
	public Polynomial(String string) {
	
		Monomial monomial;
		
		string = string.replace("-", "+-");
		
		String splits[] = string.split("\\+");
		for(String split : splits) {
			monomial = new Monomial(split);
			if(monomial.getPower() != -1) {
				monomials.add(monomial);
				
				if(monomial.getPower() > this.maxPower) {
					
					this.maxPower = monomial.getPower();
				}
			}else {
				
				this.maxPower = -1;
			}
		}
	}
	public Polynomial(Polynomial other) {
		
		this.maxPower = other.getMaxPower();
		int i = 0;
		while(i < other.getMonomials().size()) {
			
			this.monomials.add(new Monomial(other.getMonomials().get(i).getPower(), other.getMonomials().get(i).getCoefficient()));
			i++;
		}
	}
	public LinkedList<Monomial> getMonomials() {
		
		return monomials;
	}

	public Integer getMaxPower() {
		return maxPower;
	}

	public void setMaxPower(Integer maxPower) {
		this.maxPower = maxPower;
	}

	public void setMonomials(LinkedList<Monomial> monomials) {
		
		this.monomials = monomials;
	}
	
	public void addMonomial(Monomial monomial) {
		
		if(monomial.getCoefficient() == 0) {
			
			return;
		}
		
		monomials.add(new Monomial(monomial.getPower(), monomial.getCoefficient()));
		
		if(monomial.getPower() > maxPower) {
			maxPower = monomial.getPower();
		}
	}
	
	public Polynomial revert() {
		
		Polynomial result = new Polynomial();
		result.setMaxPower(maxPower);
		
		for(Monomial monomial : monomials) {
			
			result.getMonomials().addFirst(monomial);
		}
		
		return result;
	}
	
	public String showPolynomial() {
		
		String polynomialString = new String("");
		Polynomial reverted = this.revert();
		for(Monomial monomial : reverted.getMonomials()) {
			
			if(monomial == reverted.getMonomials().getLast()) {
				polynomialString += monomial.showMonomial();
			} else{
				polynomialString += monomial.showMonomial() + "+";
			}
		}
		
		return polynomialString;
	}
	
	public boolean isZero() {
		
		if(this.monomials.size() == 1 && this.monomials.getFirst().getCoefficient() == 0 && this.monomials.getFirst().getPower() == 0) {
			
			return true;
		}
		
		return false;
	}
}
