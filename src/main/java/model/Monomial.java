package model;

public class Monomial {
	
	private Integer power;
	private Double coefficient;
	
	public Monomial(Integer power, Double coefficient) {
		this.power = power;
		this.coefficient = coefficient;
	}
	
	public Monomial(String s) {
		
		if(!s.matches("(-)?([0-9])?(x)?(\\^)?([0-9])?")) {
			System.out.println();
			this.power = -1;
			return;
		}
		
		if(s.contains("x")) {
			
			String sub = s.substring(0, s.indexOf("x"));
			if(sub.isEmpty()) {
				
				this.coefficient = 1.0;
			}else if(sub.equals("-")){
				
				this.coefficient = -1.0;
			}else{
				
				this.coefficient = Double.parseDouble(sub);
			}
			
			if(s.contains("^")) {
				
				this.power = Integer.parseInt(s.substring(s.indexOf("^") + 1));
			}else {
				
				this.power = 1;
			}
		}else {
			
			this.power = 0;
			this.coefficient = Double.parseDouble(s);
		}
	}
	
	public Integer getPower() {
		return power;
	}
	
	public void setPower(Integer power) {
		this.power = power;
	}
	
	public Double getCoefficient() {
		return coefficient;
	}
	
	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}
	
	public String showMonomial() {
		
		String monomialString = new String();
		
		if(coefficient == -1) {
			
			monomialString += "-";
		}else if(coefficient < 0.0) {
			
			monomialString += coefficient.toString();
		}else if(coefficient != 1){
				
			monomialString += coefficient.toString();
		}	
		
		if(power > 1) {
			
			monomialString += "x^";
			monomialString += power.toString();
		}else if(power == 1){
			
			monomialString += "x";
		}else if(power == 0 && coefficient == 1) {
			
			monomialString += "1";
		}else if(power == 0 && coefficient == -1) {
			
			monomialString += "1";
		}
		
		return monomialString;
	}
	
}
