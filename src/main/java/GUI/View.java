package GUI;

import java.awt.*;

import javax.swing.*;

public class View extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextField polynomial1 = new JTextField("Polynomial 1");
	JTextField polynomial2 = new JTextField("Polynomial 2");
	JButton addition = new JButton("+");
	JButton subtraction = new JButton("-");
	JButton multiplication = new JButton("*");
	JButton division = new JButton("/");
	JButton derivation = new JButton("'");
	JButton integration = new JButton("S");
	JTextField result = new JTextField("Your result will be here!");
	
	Controller controller = new Controller(this);
	
	public View() {
		
		setSize(1000, 1000);
		setVisible(true);
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.ipadx = 100;
		c.weightx = 0.0;
		c.ipady = 20;
		
		c.gridwidth = 4;
		c.gridx = 0;
		c.gridy = 0;
		this.add(polynomial1, c);

		c.gridx = 5;
		c.gridy = 0;
		this.add(polynomial2, c);
		
		c.gridx = 0;
		c.gridy = 2;
		addition.addActionListener(controller);
		this.add(addition, c);
		
		c.gridx = 5;
		c.gridy = 2;
		subtraction.addActionListener(controller);
		this.add(subtraction, c);
		
		c.gridx = 0;
		c.gridy = 3;
		multiplication.addActionListener(controller);
		this.add(multiplication, c);
		
		c.gridx = 5;
		c.gridy = 3;
		division.addActionListener(controller);
		this.add(division, c);
		
		c.gridx = 0;
		c.gridy = 4;
		derivation.addActionListener(controller);
		this.add(derivation, c);
		
		c.gridx = 5;
		c.gridy = 4;
		integration.addActionListener(controller);
		this.add(integration, c);
		
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 9;
		this.add(result, c);
	}

	public JButton getAdditionButton() {
		
		return addition;
	}
	public JButton getSubtractionButton() {
		
		return subtraction;
	}
	public JButton getMultiplicationButton() {
		
		return multiplication;
	}
	public JButton getDivisionButton() {
	
		return division;
	}
	public JButton getDerivationButton() {
	
		return derivation;
	}
	public JButton getIntegrationButton() {
	
		return integration;
	}

	public JTextField getPolynomial1() {
	
		return polynomial1;
	}
	public JTextField getPolynomial2() {
	
		return polynomial2;
	}

	public JTextField getResult() {
	
		return result;
	}
}