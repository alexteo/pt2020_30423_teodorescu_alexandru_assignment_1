package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import model.*;

public class Controller implements ActionListener{

	private View view;
	private Logic logic = new Logic();
	
	public Controller(View view) {
		
		this.view = view;
	}
	
	public Polynomial stringToPolynomial(JTextField a) {
		
		String string = a.getText();
		Polynomial polynomial = new Polynomial(string);
		
		polynomial = polynomial.revert();
		
		if(polynomial.getMaxPower() == -1) {
			
			return null;
		}
		
		return polynomial;
	}

	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		Polynomial p1 = stringToPolynomial(view.getPolynomial1());
		
		if(p1 == null) {
			
			view.getResult().setText("Polynomial 1 is not correct!");
			return;
		}
		
		Polynomial result = new Polynomial();
		if(source == view.getDerivationButton()) {
			
			result = logic.derivate(p1);
			view.getResult().setText(result.showPolynomial());
			return;
		}

		if(source == view.getIntegrationButton()) {
			
			result = logic.integrate(p1);
			view.getResult().setText(result.showPolynomial() + " + C");
			return;
		}
		
		Polynomial p2 = stringToPolynomial(view.getPolynomial2());
		
		if(p2 == null) {
			
			view.getResult().setText("Polynomial 2 is not correct!");
			return;
		}
		if(source == view.getAdditionButton()) {
			
			result = logic.add(p1, p2);
		}
		
		if(source == view.getSubtractionButton()) {
			
			result = logic.subtract(p1, p2);
		}
		
		if(source == view.getMultiplicationButton()) {
					
			result = logic.multiply(p1, p2);
		}

		if(source == view.getDivisionButton()) {
			
			DivisionResult fullResult = logic.divide(p1, p2);
			if(fullResult == null) {
				
				view.getResult().setText("Can't divide by 0!");
				return;
			}
			view.getResult().setText(fullResult.showDivisionResult());
			return;
		}

		
		
		view.getResult().setText(result.showPolynomial());
	}

}
