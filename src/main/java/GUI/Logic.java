package GUI;

import functions.*;
import model.*;

public class Logic {

	private Adder adder = new Adder();
	private Subtractor subtractor = new Subtractor();
	private Multiplier multiplier = new Multiplier();
	private Divider divider = new Divider();
	private Derivator derivator = new Derivator();
	private Integrator integrator = new Integrator();
	
	public Logic() {
		
	}
	
	public Polynomial add(Polynomial p1, Polynomial p2) {
		
		return adder.addPolynomials(p1, p2);
	}
	
	public Polynomial subtract(Polynomial p1, Polynomial p2) {
		
		return subtractor.subtractPolynomials(p1, p2);
	}
	
	public Polynomial multiply(Polynomial p1, Polynomial p2) {
		
		return multiplier.multiplyPolynomials(p1, p2);
	}
	
	public DivisionResult divide(Polynomial p1, Polynomial p2) {
		
		return divider.dividePolynomials(p1, p2);
	}
	
	public Polynomial derivate(Polynomial p1) {
		
		return derivator.derivatePolynomial(p1);
	}
	
	public Polynomial integrate(Polynomial p1) {
		
		return integrator.integratePolynomial(p1);
	}

}
