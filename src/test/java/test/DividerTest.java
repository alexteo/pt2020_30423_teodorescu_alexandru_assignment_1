package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import functions.*;
import model.*;

public class DividerTest {

	@Test
	public void normalTestNoRemainder() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expectedQuotient = new Polynomial();
		Polynomial expectedRemainder = new Polynomial();
		
		DivisionResult expected = new DivisionResult();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,2.0));
		p1.addMonomial(new Monomial(2,1.0));
		
		p2.addMonomial(new Monomial(0,1.0));
		p2.addMonomial(new Monomial(1,1.0));
		
		expectedQuotient.addMonomial(new Monomial(0,1.0));
		expectedQuotient.addMonomial(new Monomial(1,1.0));
		
		expected.setQuotient(expectedQuotient);
		
		Divider divider = new Divider();
		DivisionResult actual = divider.dividePolynomials(p1, p2);
		
		Assert.assertEquals(expected.showDivisionResult(), actual.showDivisionResult());
	}

	@Test
	public void normalTestWithRemainder() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expectedQuotient = new Polynomial();
		Polynomial expectedRemainder = new Polynomial();
		DivisionResult expected = new DivisionResult();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,2.0));
		p1.addMonomial(new Monomial(2,4.0));
		p1.addMonomial(new Monomial(3,3.0));
		
		p2.addMonomial(new Monomial(0,4.0));
		p2.addMonomial(new Monomial(1,1.0));
		
		expectedQuotient.addMonomial(new Monomial(0,34.0));
		expectedQuotient.addMonomial(new Monomial(1,-8.0));
		expectedQuotient.addMonomial(new Monomial(2,3.0));
		
		expectedRemainder.addMonomial(new Monomial(0,-135.0));
		
		expected.setQuotient(expectedQuotient);
		expected.setRemainder(expectedRemainder);
		
		Divider divider = new Divider();
		DivisionResult actual = divider.dividePolynomials(p1, p2);
		
		Assert.assertEquals(expected.showDivisionResult(), actual.showDivisionResult());
	}
	
	@Test
	public void MissingMonomialTestNoRemainder() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expectedQuotient = new Polynomial();
		Polynomial expectedRemainder = new Polynomial();
		
		DivisionResult expected = new DivisionResult();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(3,-1.0));
		
		p2.addMonomial(new Monomial(0,1.0));
		p2.addMonomial(new Monomial(1,-1.0));
		
		expectedQuotient.addMonomial(new Monomial(0,1.0));
		expectedQuotient.addMonomial(new Monomial(1,1.0));
		expectedQuotient.addMonomial(new Monomial(2,1.0));
		
		expected.setQuotient(expectedQuotient);
		
		Divider divider = new Divider();
		DivisionResult actual = divider.dividePolynomials(p1, p2);
		
		System.out.println(actual.showDivisionResult());
		System.out.println(expected.showDivisionResult());
		
		Assert.assertEquals(expected.showDivisionResult(), actual.showDivisionResult());
	}

	@Test
	public void MissingMonomialTestWithRemainder() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expectedQuotient = new Polynomial();
		Polynomial expectedRemainder = new Polynomial();
		DivisionResult expected = new DivisionResult();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,1.0));
		p1.addMonomial(new Monomial(3,2.0));
		
		p2.addMonomial(new Monomial(0,1.0));
		p2.addMonomial(new Monomial(1,1.0));
		
		expectedQuotient.addMonomial(new Monomial(0,3.0));
		expectedQuotient.addMonomial(new Monomial(1,-2.0));
		expectedQuotient.addMonomial(new Monomial(2,2.0));
		
		expectedRemainder.addMonomial(new Monomial(0,-2.0));
		
		expected.setQuotient(expectedQuotient);
		expected.setRemainder(expectedRemainder);
		
		Divider divider = new Divider();
		DivisionResult actual = divider.dividePolynomials(p1, p2);
		
		Assert.assertEquals(expected.showDivisionResult(), actual.showDivisionResult());
	}
}
