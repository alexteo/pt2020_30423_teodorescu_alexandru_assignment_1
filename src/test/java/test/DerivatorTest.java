package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import functions.Adder;
import functions.Derivator;
import model.Monomial;
import model.Polynomial;

public class DerivatorTest {

	@Test
	public void normalTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,2.0));
		p1.addMonomial(new Monomial(2,4.0));
		
		expected.addMonomial(new Monomial(0,2.0));
		expected.addMonomial(new Monomial(1,8.0));
		
		Derivator derivator = new Derivator();
		
		Polynomial actual = derivator.derivatePolynomial(p1);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

	@Test
	public void missingMonomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		expected.addMonomial(new Monomial(1,4.0));
		expected.addMonomial(new Monomial(2,12.0));
		
		Derivator derivator = new Derivator();
		
		Polynomial actual = derivator.derivatePolynomial(p1);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
}
