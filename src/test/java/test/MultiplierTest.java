package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import functions.*;
import model.*;

public class MultiplierTest {

	@Test
	public void normalTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,2.0));
		p1.addMonomial(new Monomial(2,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(1,2.0));
		p2.addMonomial(new Monomial(2,1.0));
		
		expected.addMonomial(new Monomial(0,5.0));
		expected.addMonomial(new Monomial(1,12.0));
		expected.addMonomial(new Monomial(2,25.0));
		expected.addMonomial(new Monomial(3,10.0));
		expected.addMonomial(new Monomial(4,4.0));
		
		Multiplier multiplier = new Multiplier();
		
		Polynomial actual = multiplier.multiplyPolynomials(p1, p2);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

	@Test
	public void missingMonomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(2,2.0));
		p2.addMonomial(new Monomial(3,1.0));
		
		expected.addMonomial(new Monomial(0,5.0));
		expected.addMonomial(new Monomial(2,12.0));
		expected.addMonomial(new Monomial(3,21.0));
		expected.addMonomial(new Monomial(4,4.0));
		expected.addMonomial(new Monomial(5,10.0));
		expected.addMonomial(new Monomial(6,4.0));
		
		Multiplier multiplier = new Multiplier();
		
		Polynomial actual = multiplier.multiplyPolynomials(p1, p2);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
	
	@Test
	public void differentSizeTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(1,2.0));
		p2.addMonomial(new Monomial(2,1.0));
		
		expected.addMonomial(new Monomial(0,5.0));
		expected.addMonomial(new Monomial(1,2.0));
		expected.addMonomial(new Monomial(2,11.0));
		expected.addMonomial(new Monomial(3,24.0));
		expected.addMonomial(new Monomial(4,10.0));
		expected.addMonomial(new Monomial(5,4.0));
		
		Multiplier multiplier = new Multiplier();
		
		Polynomial actual = multiplier.multiplyPolynomials(p1, p2);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
	@Test
	public void emptyPolynomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		
		expected.addMonomial(new Monomial(0,1.0));
		expected.addMonomial(new Monomial(2,2.0));
		expected.addMonomial(new Monomial(3,4.0));
		
		Multiplier multiplier = new Multiplier();
		
		Polynomial actual = multiplier.multiplyPolynomials(p1, p2);
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

}
