package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import functions.Derivator;
import functions.Integrator;
import model.Monomial;
import model.Polynomial;

public class IntegratorTest {

	@Test
	public void normalTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,1.0));
		p1.addMonomial(new Monomial(2,12.0));
		
		expected.addMonomial(new Monomial(1,1.0));
		expected.addMonomial(new Monomial(2,0.5));
		expected.addMonomial(new Monomial(3,4.0));
		
		Integrator integrator = new Integrator();
		
		Polynomial actual = integrator.integratePolynomial(p1);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

	@Test
	public void missingMonomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,6.0));
		p1.addMonomial(new Monomial(3,3.0));
		
		expected.addMonomial(new Monomial(1,1.0));
		expected.addMonomial(new Monomial(3,2.0));
		expected.addMonomial(new Monomial(4,0.75));
		
		Integrator integrator = new Integrator();
		
		Polynomial actual = integrator.integratePolynomial(p1);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
}
