package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import functions.*;
import model.*;

public class SubtractorTest {

	@Test
	public void normalTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(1,2.0));
		p1.addMonomial(new Monomial(2,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(1,2.0));
		p2.addMonomial(new Monomial(2,1.0));
		
		expected.addMonomial(new Monomial(0,-4.0));
		expected.addMonomial(new Monomial(2,3.0));
		
		Subtractor subtractor = new Subtractor();
		
		Polynomial actual = subtractor.subtractPolynomials(p1, p2);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

	@Test
	public void missingMonomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(2,2.0));
		p2.addMonomial(new Monomial(3,1.0));
		
		expected.addMonomial(new Monomial(0,-4.0));
		expected.addMonomial(new Monomial(3,3.0));
		
		Subtractor subtractor = new Subtractor();
		
		Polynomial actual = subtractor.subtractPolynomials(p1, p2);
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
	
	@Test
	public void differentSizeTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		p2.addMonomial(new Monomial(0,5.0));
		p2.addMonomial(new Monomial(1,2.0));
		p2.addMonomial(new Monomial(2,1.0));
		
		expected.addMonomial(new Monomial(0,-4.0));
		expected.addMonomial(new Monomial(1,2.0));
		expected.addMonomial(new Monomial(2,1.0));
		expected.addMonomial(new Monomial(3,4.0));
		
		Subtractor subtractor = new Subtractor();
		
		Polynomial actual = subtractor.subtractPolynomials(p1, p2);
		System.out.println(actual.showPolynomial());
		
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}
	@Test
	public void emptyPolynomialTest() {
		
		Polynomial p1 = new Polynomial();
		Polynomial p2 = new Polynomial();
		Polynomial expected = new Polynomial();
		
		p1.addMonomial(new Monomial(0,1.0));
		p1.addMonomial(new Monomial(2,2.0));
		p1.addMonomial(new Monomial(3,4.0));
		
		
		expected.addMonomial(new Monomial(0,1.0));
		expected.addMonomial(new Monomial(2,2.0));
		expected.addMonomial(new Monomial(3,4.0));
		
		Subtractor subtractor = new Subtractor();
		
		Polynomial actual = subtractor.subtractPolynomials(p1, p2);
		Assert.assertEquals(expected.showPolynomial(), actual.showPolynomial());
	}

}
